package com.boat.config;

import java.util.Properties;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.boat.kafka.consume.receiver.KafkaMessageReceiver;


/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2018年1月12日 上午9:02:35
 * @version V1.0
 */

@Configuration
public class KafkaReceiverBean {
	
	@Resource
	private Properties kafkaComsumerProperties;

	@Value("${kafka.consumer.message-numPartitions}")
	private int messageNumPartitions;

	@Value("${kafka.consumer.message-topic}")
	private String messageTopic;

	@Bean(initMethod = "init", destroyMethod = "destroy")
	public KafkaMessageReceiver kafkaMessageReceiver() {
		KafkaMessageReceiver kafkaMessageReceiver = new KafkaMessageReceiver();
		kafkaMessageReceiver.setProperties(kafkaComsumerProperties);
		kafkaMessageReceiver.setPartitionsNum(messageNumPartitions);
		kafkaMessageReceiver.setTopics(messageTopic);
		return kafkaMessageReceiver;
	}
}
