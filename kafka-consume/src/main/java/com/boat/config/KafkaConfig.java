package com.boat.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2018年1月11日 下午3:04:45
 * @version V1.0
 */

@Configuration
public class KafkaConfig {
	

	@Value("${kafka.consumer.bootstrapServers}")
	private String bootstrapServers;

	@Value("${kafka.consumer.groupId}")
	private String groupId;

	@Value("${kafka.consumer.intervalMs}")
	private int intervalMs;

	@Value("${kafka.consumer.timeoutMs}")
	private int timeoutMs;

	@Value("${kafka.consumer.autoOffset}")
	private String autoOffset;

	@Value("${kafka.consumer.keyDeserializer}")
	private String keyDeserializer;

	@Value("${kafka.consumer.valueDeserializer}")
	private String valueDeserializer;

	@Value("${kafka.consumer.autoCommit}")
	private boolean autoCommit;

	@Value("${kafka.consumer.pollRecords}")
	private int pollRecords;

	@Bean("kafkaComsumerProperties")
	public Properties kafkaComsumerProperties() {
		Properties properties = new Properties();
		properties.put("bootstrap.servers", bootstrapServers);
		properties.put("group.id", groupId);
		properties.put("auto.commit.interval.ms", intervalMs);
		properties.put("session.timeout.ms", timeoutMs);
		properties.put("auto.offset.reset", autoOffset);
		properties.put("key.deserializer", keyDeserializer);
		properties.put("value.deserializer", valueDeserializer);
		properties.put("enable.auto.commit", autoCommit);
		properties.put("max.poll.records", pollRecords);
		return properties;
	}

	
}
