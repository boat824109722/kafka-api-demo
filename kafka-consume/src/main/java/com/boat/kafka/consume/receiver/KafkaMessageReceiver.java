package com.boat.kafka.consume.receiver;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.boat.kafka.consume.KafkaMessageConsumer;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2018年2月4日 下午5:06:03
 * @version V1.0
 */

public class KafkaMessageReceiver extends KafkaMessageConsumer {

	private final Logger logger = LoggerFactory.getLogger(KafkaMessageReceiver.class);

	@Override
	public void processMessage(List<String> messageList) {
		int messageListSize = messageList.size();
		if (messageList != null && messageList.size() > 0) {
			for (int i = 0; i < messageListSize; i++) {
				logger.info(messageList.get(i));
			}
		}
	}
}
