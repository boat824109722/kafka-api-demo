package com.boat.bean;

/**   
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author boat 
 * @date 2018年2月5日 上午9:49:44 
 * @version V1.0   
 */

public class Message {
	
	private String title;
	
	private String content;
	
	private String formEmail;
	
	private String toEmail;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getFormEmail() {
		return formEmail;
	}

	public void setFormEmail(String formEmail) {
		this.formEmail = formEmail;
	}

	public String getToEmail() {
		return toEmail;
	}

	public void setToEmail(String toEmail) {
		this.toEmail = toEmail;
	}
	
}
