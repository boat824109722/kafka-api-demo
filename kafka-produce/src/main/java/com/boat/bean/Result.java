package com.boat.bean;

/**
 * @author Administrator
 *
 */
public class Result {

	private boolean success = true;// 是否成功
	private String msg;// 信息
	private Object obj;// 返回对象
	private int code;// 状态代码

	public Result() {
	}

	public Result(boolean success, String msg, Object obj, int code) {
		this.success = success;
		this.msg = msg;
		this.obj = obj;
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public Object getObj() {
		return obj;
	}

	public void setObj(Object obj) {
		this.obj = obj;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
