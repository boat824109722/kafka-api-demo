package com.boat.controller;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.boat.bean.Message;
import com.boat.bean.Result;
import com.boat.kafka.produce.KafkaMessageProducer;

/**
 * @Description: TODO(用一句话描述该文件做什么)
 * @author boat
 * @date 2018年2月4日 下午5:46:32
 * @version V1.0
 */

@Scope("prototype")
@RestController
@RequestMapping(value = "/messageProducer")
public class MessageProducerController {

	@Resource
	private KafkaMessageProducer kafkaMessageProducer;

	@PostMapping("/sendMessage")
	public Result sendMessage(@RequestBody Message message) {
		String strMessage = JSON.toJSONString(message);
		kafkaMessageProducer.sendText(strMessage, true);
		return new Result(true, "send success!", null, 200);
	}
}
