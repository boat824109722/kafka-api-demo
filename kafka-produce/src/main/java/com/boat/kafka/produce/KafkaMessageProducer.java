package com.boat.kafka.produce;

import java.util.List;

import com.boat.kafka.exception.JmsException;

public interface KafkaMessageProducer {


	/**
	 * 发送字符串
	 * @param message
	 */
	public void sendText(String message,boolean flush) throws JmsException;


    /**
	 * 发送字符串集合
	 * @param message
	 * @throws JmsException
	 */
	public void sendText(List<String> message,boolean flush) throws JmsException;
	
	/**
	 * 按分区发送发送字符串
	 * @param message
	 * @param flush
	 * @param keyToPartition
	 * @throws JmsException
	 */
	public void sendText(String message, boolean flush, String keyToPartition) throws JmsException;

}