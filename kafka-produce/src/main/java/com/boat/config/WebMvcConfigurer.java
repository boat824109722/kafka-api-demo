package com.boat.config;

import java.nio.charset.Charset;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;

/**
 * Spring MVC 配置
 */

@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {

	// 使用阿里 FastJson 作为JSON MessageConverter
//	@Override
//	public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//		FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
//		FastJsonConfig config = new FastJsonConfig();
//		config.setSerializerFeatures(SerializerFeature.WriteMapNullValue, // 保留空的字段
//				SerializerFeature.WriteNullStringAsEmpty, // String null -> ""
//				SerializerFeature.WriteNullNumberAsZero);// Number null -> 0
//		converter.setFastJsonConfig(config);
//		converter.setDefaultCharset(Charset.forName("UTF-8"));
//		converters.add(converter);
//	}

	// 添加拦截器
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new CorsInterceptor()).addPathPatterns("/**");
		super.addInterceptors(registry);
	}

	
}
